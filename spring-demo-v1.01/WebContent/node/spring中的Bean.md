#spring中的bean#

- 掌握实例化Bean的三种方式（三种装配方式）
- 熟悉Bean的作用域和生命周期
- Bean的常用属性及其子元素

###Bean的配置###

spring中的Bean简介：
如果把spring看做一个大型的工厂，则Spring容器中的Bean就是该工厂的产品。要想使用这个工厂生产和管理Bean，就需要在配置文件中告诉它需要哪些Bean，以及需要使用和种方式讲这些Bean装配到一起。

Bean的本质就是java中的类，而Spring中的Bean其实就是对实体类的引用，来生产Java类对象，从而实现生产和管理Bean。

	注：就好比一个汽车工厂，所有的生产的汽车都是在生产线上流水操作，进行批量生产。而我们所学的这个spring就是这个汽车工厂，流水线就好比我们Bean，拥有生产各种汽车零部件的模具。最后我们应该怎么在使用这些Bean生产出来的零件，就需要spring告诉机器（或者人工）这些零件的组装方法。
	在我们的项目中，之前我们所使用的实体类，都是在使用的进行new，对程序的内存造成了极大的浪费，（虽然在java中有JC，但是JC的处理机制就好比：小区的垃圾是一天一清，而不是及时清理）利用spring工厂创建出来的对象可以有单例...可以极大节省程序的内存，并且对程序的耦合性有很好的的支持。
	
spring容器支持两种格式的**配置文件**

- properties文件
- XML文件

在实际开发中，最常用的是Xml文件格式的配置方式，这种配置方式是通过Xml文件来注册并管理Bean之间的依赖关系。

XMl配置文件的根元素是<beans>,<beans>中包含了多个<bean>子元素，每个<bean>子元素定义了一个Bean，并描述了改Bean如何被装配到spring容器中，关于<beans>元素常用的属性如下表所示：

>详见P16页 表2-1<bean>元素的常用属性及其子元素

注：如果在bean中未指定id和name，则spring会将class值当做id使用

###Bean的实例化###

- 构造器实例化（最常见）
- 静态工厂方式实例化
- 实例工厂方式实例化

### Bean构造器实例化###

**构造器实例化**是指Spring容器通过Bean对应的类中默认的构造函数来实例化Bean。

>详见com.sun.contructor包中的内容。

### Bean静态工厂方法实例化 ###
**静态工厂**是实例化Bean的另一种方式。该方式要求自己创建一个静态工厂的方法来创建Bean的实例。

>详见com.sun.static_factory包中的内容

###Bean实例工厂方式实例化###
**实例工厂**是采用直接创建Bean实例的方式，在配置文件中，通过factory-bean属性配置一个实例工厂，然后使用factory-method属性确定使用工厂中的那个方法

#Bean作用域#
spring4.3中为bean的实例化定义了7中作用域

- singleton(单例):使用singleton定义的bean在spring容器中将只有一个实例，也就是说，无论有多少个Bean引用它，始终指向**同一个对象**。这也是spring容器中默认的作用域。
- prototype(原型):每次通过spring容器获取的prototype定义的Bean时，容器都将**创建一个新的bean实例**
- request：再一次http请求中容器会返回改Bean的同一个实例。对不同的Http请求泽辉产生一个新的Bean，而且该Bean近在当前Http request内有效
- session：再一次httpsession中，容器会返回该Bean的同一个实例。对不同的HTTP请求则会产生一个新的Bean，而且该bean仅在当前HTTPSession内有效
- globalSession：在全局的HTTPsession中，容器会返回该Bean的同一个实例。仅在使用porlet上下文时有效。
- Application ：为每个ServletContext 对象创建一个实例。仅当在ApplicationContext中生效。
- websocket：为每个websocket对象创建一个实例。仅在web相关的ApplicationContext中生效。

###singleton作用域###
是spring容器默认的作用域，当Bean的作用域为singleton时，spring容器就只会存在一个共享的Bean实例。singleton作用域对于无会话状态的Bean（如Dao组件、service组件）来说是最理想的选择。
**spring会全程跟踪singleton作用域的bean的生命周期**

>详见com.sun.scope包


###prototype作用域###
对**需要保持会话状态的Bean**（如Struts2的Action类），应该使用*prototype作用域*。在使用prototype作用域时，spring容器会为每个对该Bean的请求都创建一个新的实例

>详见com.sun.scope包，通过修bean4.xml中的配置，测试可获得不一样的结果

##Bean的生命周期##
了解spring中Bean的生命周期有何意义？

了解spring中Bena的生命周期的意义就在于，可以利用Bean在其存货器件的特定时刻完成一些相关操作、这种时刻可能有很多，但一般情况下，常在Bean的*postinitiation（初始化后）*和*predestruction（销毁前）*执行一些相关操作

spring容器可以管理Bean部分作用域的生命周期。如下图所示：
![模块划分](../../img/2020-09-18_172833.png)


![模块划分](../../img/2020-09-18_174319.png)

>代码com.sun.life

##Bean的装配方式##

- 基于xml的装配
- 基于注解（Annotation）的装配
- 自动装配

#####什么是Bean的装配?#####

Bean的装配可以理解为依赖关系注入，Bean的装配方式即Bean*依赖注入*的方式。

###基于xml的装配###
>代码 com.sun.assemble

![模块划分](../../img/2020-09-22_111030.png)

	注：基于xml的装配可能会导致xml配置文件过于庞大，不利于后期的维护

###基于Annotation的装配###

- @Component：用于描述spring中的Bean，它是一个泛化的概念，仅仅表示一个组件
- @Repository：用于将数据访问层（DAO）的类标识为spring中的Bean
- @Service：用于将业务层（Service）的类标识为spring中的Bean
- @Controller：用于将控制层（Controller）的类标识为spring中的Bean
- @Autowired：用于对Bean的属性变量、属性的setter方法及构造方法进行标注，配合对应的注解处理器完成Bean的自动配置工作。
- @Resource：其作用与Autowired一样。@Resource中有两个重要属性：name和type。spring将name属性解析为Bean实例名称，type属性解析为Bean实例类型。
- @Qualifier：与@AutoWired注解配合使用，会将默认的按Bean类型装配修改为按Bean实例名称装配，Bean的实例名称由@Qualifier注解的参数指定

> 代码com.sun.annotation包

###自动装配###
所谓的自动装配，就是将一个Bean自动注入到其他Bean的Property中。spring的<bean>元素中包含一个autowirte属性，我们可以通过设置autowirte的属性值来自动装配Bean。

autowirte属性有5个值，说明如下：
- default 由<bean>的上级标签<bean>的default-autowirte属性值确定。
- byName 根据属性的名和产能自动装配。容器将根据名臣查找与属性完全一致的Bean，并将属性自动装配
- byType 根据属性的数据类型（Type）自动装配，如果一个Bean的数据类型，兼容另一个Bean中枢性的数据类型，则自动装配
- constructor 根据构造函数参数的数据类型，进行byType模式的自动装配
- no 默认情况下，不使用自动装配，Bean依赖必须通过ref元素定义

	注：这种方式不常用，所以没有代码

















