#AspectJ开发#
AspectJ是一个基于java语言的AOP框架，它提供了强大的AOP功能。spring2.0之后，springAOP引入了对AspectJ的支持，并允许直接使用AspectJ进行编程，而spring自身的AOPAPI也尽量与AspectJ保持一致。

使用AspectJ实现AOP有两种方式：

- 基于XML的声明式AspectJ
- 基于注解的声明式AspectJ

###基于XML的声明式AspectJ###

基于XML的声明式AspectJ是指通过XMl文件来定义切面、切入点及通知，所有的切面、切入点和通知都必须定义在<aop:config>元素内。

![模块划分](../../img/2020-09-23_231321.png)

![模块划分](../../img/2020-09-23_231829.png)

1.配置切面

在spring的配置文件中，配置切面使用的是<aop:aspect>元素，改元素会将一个已定义好的spring Bean转换成切面Bean，所哟要在配置文件中先定义一个普通的spring Bean

配置<aop:aspect>元素时，通常会指定id和ref两个属性
- id：用于定义该企鹅面的唯一标识名称
- ref：用于引用普通的Spring Bean


2.配置切入点

当<aop:pointcut>元素作为<aop:config>元素的子元素定义时，表示该切入点是全局切入点，它可以被多个切面所共享；当<aop:pointcut>元素作为<aop:aspect>元素子元素时，表示该切入点支队当前切面有效。

在定义<aop:pointcut>元素时，通常会指定id和expression两个属性。
- id：用于指定切入点的唯一标识名称
- expression： 用于指定切入点关联的切入点表达式

3.切入点表达式

execution(\* com.itheima.jdk.\*.*(..))是定义的切入点表达式，该切入点表达式的意思是匹配com.itheima.jdk包中的任意类的任意方法的执行。
- \*： 返回类型，使用\*代表所有类型
- com.itheima.jdk：需要拦截的包名
- \*： 类名，使用\*代表所有类
- \*：方法名，使用\*代表所有方法
- (..)：其中“..”表示任意参数
	
	注意：返回类型与拦截的包中间有一个空格
![模块划分](../../img/2020-09-24_000520.png)

4.配置通知

使用<aop:aspect>的子元素可以配置5中常用的通知，这5个子元素不支持使用子元素，但是在使用时可以指定一些属性，其常用属性及其描述如下：
- pointcut 该属性用于指定一个切入点表达式，spring将在匹配该表达式的连接电视织入该通知。
- pointcut-ref 该属性指定一个已经存在的切入点名称，如配置代码中的myPointCut。通常pointcut和pointcut-ref两个属性只需使用一个就可以。
- method 该属性指定一个方法名，指定将切面bean中的该方法转换为增强处理。
- throwing 该属性只对<after-trowing>元素有效，它用于指定一个形参名，异常通知方法可以通过该形参访问目标方法所抛出的异常。
- returning 该属性只对<after-returning>元素有效，它用于指定一个形参名，后置通知方法可以通过该形参访问目标方法的返回值。

###基于注解的声明式AspectJ###

AspectJ框架为AOP的实现提供了一套注解，用以取代Spring配置文件中为实现AOP功能所配置的臃肿代码。AspectJ注解及其描述如下：
- @Aspect：用于定一个切面
- @Pointcut：用于定义切入点表达式。
- @Before：用于定义前置通知
- @AfterReturning：用于定义后置通知
- @Around：用于定义环绕通知
- @AfterThrowing：用于定义异常通知来处理程序中未处理的异常
- @After：用于定义最终的final通知
- @DeclareParents 用于定义引介通知
