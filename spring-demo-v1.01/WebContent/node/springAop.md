#Spring AOP#
- spring AOP简介
- 动态代理
- 基于代理类的AOP实现
- AspectJ开发

###AOP简介###
aop的全称是Aspect-OrientedProgramming，即面向切面编程（也称面向方面编程）。它是面向对象编程oop的一种补充，目前已成为一种比较成熟的编程方式。

aop的由来：再传通的业务处理代码中，通常都会进行事务处理、日志记录等操作。虽然使用oop可以通过组合或者继承的方式来达到代码的重用，但如果实现某个功能（如日志记录），同样的代码仍然会分散到各个方法中。这样，如果想要关闭某个功能，或者队旗进行修改，就必须要修改所有的相关方法。这不但增加了开发人员的工作量，而且提高了代码的出错率。

解决方案：aop思想随之产生，aop采取**横向抽取机制**，将分散在各个方法中的重复代码提取出来，然后在程序编译或运行时，再讲这些提取出来的代码应用到需要执行的地方。这种采用横向抽取机制的方式，采用传统的oop思想显然是无法实现的。因为oop只能实现父子关系的纵向的重用。虽然Aop是一种新的编程思想，但却不是**oop的替代品**，他只是**oop的延伸和补充**。

![模块划分](../../img/2020-09-22_231220.png)

	aop的使用，是开发人员在编写业务逻辑时可以专心于核心业务，而不用过多的关注与其他业务逻辑的实现，这不但提高了开发效率，而且增强了代码的可维护性。

###AOP术语###
![模块划分](../../img/2020-09-22_231220.png)
- Aspect（切面）：封装的用于横向插入系统功能（如事务
日志等）的类
- Joinpoint（连接点）：在程序执行过程中的某个阶段点
- pointcut（切入点）： 切面与程序流程的交叉点，及那些需要处理的连接点
- Adivce（通知/增强处理）：AOP框架在特定的切入点执行的增强处理，即在定义好的切入点索要执行的程序代码。可以将其理解为切面类中的方法。
- TargetObject（目标对象）：指所有被通知的对象，也被称为增强对象。如果AOP框架采用的是动态的AOP实现，那么该对象就是一个被代理对象。
- Proxy（代理）：将通知应用到目标对象之后，被动态创建的对象。
- Weaving（织入）：将切面代码插入到目标对象上，从而生成代理对象的过程。

![模块划分](../../img/2020-09-22_231905.png)

###JDK动态代理###

JDK动态代理是通过java.lang.reflect.Proxy类来实现的，我们可以调用Proxy类的newProxyInstance（）方法来创建代理对象。对于使用**业务接口的类**，spring默认会使用JDK动态代理来实现AOP

>代码：com.sun.aspect切面包 存放切面方法    com.sun.jdk 代理代码实现及详解


	JDK的动态代理使用起来非常简单，但是它是有局限性的，使用动态代理的对象《必须实现一个或多个接口》。

###CGLIB代理###
如果代理没有实现接口的类呢？比如controller包下的所有类都没有实现接口。那么应该如何实现切面的插入。

CGLIB（Code Generation Library） 是一个高性能开源的代码生成包，它采用非常底层的字节码技术，对指定的目标类生成一个子类，并对子类进行增强。

>代码：com.sun.aspect 存放切面类及方法    com.sun.cglib 代理代码实现及详解

###spring的通知类型###

spring按照通知在目标类方法的连接点位置，可以分为5种类型，具体如下：

- 前置通知[Before advice]：在连接点前面执行，前置通知不会影响连接点的执行，除非此处抛出异常。 
- 正常返回通知[After returning advice]：在连接点正常执行完成后执行，如果连接点抛出异常，则不会执行。 
- 异常返回通知[After throwing advice]：在连接点抛出异常后执行。
- 返回通知[After (finally) advice]：在连接点执行完成后执行，不管是正常执行完成，还是抛出异常，都会执行返回通知中的内容。 
- 环绕通知[Around advice]：环绕通知围绕在连接点前后，比如一个方法调用的前后。这是最强大的通知类型，能在方法调用前后自定义一些操作。环绕通知还需要负责决定是继续处理join point(调用ProceedingJoinPoint的proceed方法)还是中断执行

###ProxyFactoryBean###

ProxyFactoryBean是FactoryBean接口的实现类，FactoryBean负责实例化一个Bean，而ProxyFactoryBean负责为期他Bean创建代理实例。在spring中，使用ProxyFactoryBean时创建AOP代理的基本方式。

ProxyFactoryBean类中的常用可配置属性如下：
- target 代理的目标对象
- proxyInterfaces 代理要实现接口，如果多个接口，可以使用一下格式赋值<list> <value></value>....</list>
- proxyTargetClass 是否对类代理而不是接口，设置为true时，使用CGLIB代理
- interceptorName 需要织入魔表的Advice
- singleton 返回的代理是否为单例，默认为true（即返回单例）
- optmize 当设置为true是，强制使用CGLIB


