#spring的核心容器#
spring容器会负责控制程序之间的关系，而不是由程序代码直接控制。spring为我提供了*两种核心容器*，分别是*BeanFactory*和*ApplicationContext*

##BeanFactory##
创建BeanFactory实例是没需要提供Spring所管理容器的详细配置信息，这些信息通常采用XML文件形式来管理，器加载配置信息的语法如下：

	BeanFactory bean= new XMLBeanFactory（new FileSystemResource（"文件路径"））；
	
注：这种方式在实际开发中不用。

##ApplicationContext##
**详见com.sun.dao.TestIoc中的**

##依赖注入##
DI的全称是Dependency Injection，中文称之为依赖注入。他与控制反转（Ioc）的含义相同，只不过这两称呼是从两个角度描述同一个概念。

IOC：在使用spring框架之后，对象的实例不在有调用者来创建，而是由spring容器来创建，spring容器会负责控制程序之间的关系，而不是由调用者的程序代码直接控制。这样，控制权有应用代码转移到了spring容器，控制权发生了反转，这就是控制反转。

DI：从spring容器的角度来看，spring容器负责将被依赖对象赋值给调用者的成员变量，这相当于为调用者注入了它依赖的实例，这就是spring的依赖注入。
###两种注入的方式###
- setter方法注入
- 构造方法注入