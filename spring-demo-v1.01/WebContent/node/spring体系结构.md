#spring的体系结构#

spring框架才用的分层架构，他一系列的功能要素被分成20个模块
![模块划分](../..//img/2020-09-16_103441.png)
注意：上图中灰色背景模块是我们主要学习的模块

##Core Container（核心容器）##
	Beans：提供了BeanFactory，spring将管理对象称为Bean
	core：提供了spring框架的基本组成部分，包括IOC和DI功能
	Context：建立在Core和Beans模块的基础之上，它是访问定义和配置的任何对象的媒介。
	SpEl：spring3.0后新增的模块，是运行时查询和操作对象图的强大的表达式语言。
##Data Access/Integration（数据访问/集成)##
	JDBC:提供了一个JDBC的抽闲层，大幅度的减少了在开发过程中对数据库操作的编码。（这个JDBC是spring对java中所讲的JDBC抽象层的一个封装）
	ORM:对流行的对象关系映射API，包括JPA，JDO和Hibernate提供了集成层支持。
	Transcations:支持对实现特殊接口以及所有POJO类的编程和声明式的事务。
	OXM：提供了一个支持对象/xml映射的抽象层实现，ruJAXB、Castor、XMLBeans、JiBx和XStream。（了解）
	JMS：指java的消息传递服务，包含使用和产生信息的特性，自4.1版本后支持与Sping-message模块的集成。（了解）
##web##
	websocket：spring4.0以后新增的模块，他提供了WebSocket和ScokJS的实现，以及对STOMP的支持。（了解）
	Servlet：也称Spring-webMvc模块，包含Spring模型-视图-控制器（MVC）和RESTWeb Services实现的web程序
	web：提供了基本的web开发集成，如；多文件上传、使用Servlet监听器来初始化Ioc容器和Web应用上下文。
	porlet：提供了在porlet环境中使用MVC实现，类似Servlet模块的功能（了解）
##其他模块##
	AOP：提供了面向切面编程实现，允许定义方法拦截器和切入点，将代码按照功能尽心分离，以降低耦合性
	Aspects：提供了与AspectJ的集成功能，Aspectj是一个功能强大切成熟的面向切面编程（AOP）框架
	Instrumentation：提供了类工具的支持和类加载器的实现，可以在特定的应用服务器中使用。（了解）
	Messaging：spring4.0以后新增，他提供了对消息传递体系结构和协议支持。（了解）
	Test：提供了对单元测试和集成测试的支持。（了解）
#spring的下载目录结构#
	spring开发所需的jar包分为两个部分：*spring框架包* 和 *第三方依赖包*。
	
	libs：JAR包和源码
- 以.RELEASE.jar结尾的是class文件JAR包	
- 以.RELEASE-javadac.jar结尾的是API文档压缩包
- 以.RELEASE-source.jar结尾是源文件压缩包

	docs：API文档和开发规范
	Schema：开发所需的schema文件

##lib目录下的四个spring的基础包##

- **spring-core-4.3.6.RELEASE.jar**

	包含spring框架的核心工具类  spring其他组件都要用到这个包里的类
- **spring-beans-4.3.6-RELASE.jar**

	所有应用都要用到的JAR包 它包含访问配置文件、创建和管理Bean以及进行控制反转或者依赖注入操作相关的所有类
- **spring-context-4.3.6.RELEASE.jar**

	提供了在基础IoC功能上的扩展服务，还提供了许多企业级服务的支持
- **spring-expression-4.3.6.RELEASE.jar**

	定义了spring的表达式语言

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	