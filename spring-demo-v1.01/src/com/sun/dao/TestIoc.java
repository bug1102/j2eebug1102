package com.sun.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * ApplicationContext 是 BeanFactory 的子接口，是另一种常用的spring核心的容器。
 * 包含了BeanFactory的所有功能
 * 还添加了对国际化、资源访问、时间传播等方面的支持。
 * 创建ApplicationContext接口实例，通常采用两种方法，具体如下：代码实现
 * @author stl
 *
 */
public class TestIoc {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		/**
		 * 在java项目中这么配置
		 */
		//1.通过ClassPathXmlApplicationContext创建ApplicationContext
		//ApplicationContext会从类路径classPath中寻找制定的XMl配置文件，找到并转载完成ApplicationContext的实例化工作
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//2.通过FileSystemXMLApplicationContext会从指定的文件系统路径（绝对路径中寻找指定的XML配置文件，找到并装载完成ApplicationContext的实例化工作
		//ApplicationContext ctx = new FileSystemXmlApplicationContext("绝对路径");
		/**
		 * 在web项目中通常会使用Contextloaderlistener来实现，
		 * 此种方式只需要在web.xml中添加相应配置即可
		 * web项目中详解
		 */
		
		/**
		 * 创建spring容器后，就可以获取spring容器中的bean。
		 * spring获取bean的实例通常采用以下两种方法
		 * 1、根据容器中Bean的id或者那么来获取指定的bean，获取之后需要强转
		 * getBean(String name)
		 * 2、根据类型来获取Bean的实例。由于此方法为泛型方法，因此在获取bean之后不需要强转
		 * <T>T getBean(Class <T>requiredType)
		 */
		//这是第一种
		UserDao bean = (UserDao) ctx.getBean("userDao");
		//第二种
		UserDao bean2 = ctx.getBean(UserDao.class);
		bean2.say();
		bean.say();
	}
}
