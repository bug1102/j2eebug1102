package com.sun.factory;

public class MyBean3Factory {

	public MyBean3Factory() {
		System.out.println("MyBean3Factory正在实例化");
	}
	
	public Bean3 createBean() {
		return new Bean3();
	}
}
