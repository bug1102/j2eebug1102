package com.sun.factory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InstanceTest {
	public static void main(String[] args) {
		String xmlPath = "com/sun/factory/beans3.xml";
		ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPath);
		System.out.println(ctx.getBean("bean3"));

	}
	
}
