package com.sun.jdk;

public interface UserDao {

	public void addUser();
	
	public void deleteUser();
}
