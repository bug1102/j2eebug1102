package com.sun.scope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		String xmlPath = "com/sun/scope/beans4.xml";
		ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPath);
		System.out.println(ctx.getBean("scope"));
		System.out.println(ctx.getBean("scope"));
	}

}
