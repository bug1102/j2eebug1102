package com.sun.life;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LifeTest {
	/**
	 * 声明周期执行流程实例：
	 * 实例化
	 * 初始化方法
	 * 消费方法
	 * @param args
	 */
	
	public static void main(String[] args) {
		//定义配置文件路径
		String xmlPath = "com/sun/life/life.xml";
		//加载配置文件时，对Bean进行实例化
		  ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPath);
		//ctx提供了关闭容器的方法，这样bean的销毁方法才会被调用
		ctx.close();

	}

}
