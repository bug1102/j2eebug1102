package com.sun.cglib;

public class CglibTest {

	public static void main(String[] args) {
		//创建代理对象
		CglibProxy cgl = new CglibProxy();
		//创建目标对象
		UserDao userDao = new UserDao();
		//获取增强后的目标对象
		UserDao createProxy = (UserDao) cgl.createProxy(userDao);
		
		createProxy.addUser();
		createProxy.deleteUser();
		
	}

}
