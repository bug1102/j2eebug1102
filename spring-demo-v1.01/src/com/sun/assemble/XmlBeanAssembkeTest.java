package com.sun.assemble;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlBeanAssembkeTest {

	
	public static void main(String[] args) {
		//定义配置文件路径
		String xmlPath = "com/sun/assemble/beans5.xml";
		//加载配置文件
		ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPath);
		//构造方法输出结果
		System.out.println(ctx.getBean("user1"));
		//设置方式输出结果
		System.out.println(ctx.getBean("user2"));
		
	}

}
