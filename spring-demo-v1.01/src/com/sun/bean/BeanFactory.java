package com.sun.bean;
/**
 * 一个工厂当中可以生产出不同的bean
 * 
 * @author stl
 *
 */
public class BeanFactory {

	public static Bean2 createBean2() {
		return new Bean2();
	}
	public static Bean3 createBean3() {
		return new Bean3();
	}
}
