package com.sun.contructor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InstanceTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		//定义配置文件的路径
		String  xmlPath = "com/sun/contructor/beans1.xml";
		//ApplicationContext在加载配置文件时，对Bean进行实例化
		ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPath);
		Bean1 bean = (Bean1) ctx.getBean("bean1");
		System.out.println(bean);
	}
}
