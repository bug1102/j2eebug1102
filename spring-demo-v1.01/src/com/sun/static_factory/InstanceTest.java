package com.sun.static_factory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InstanceTest {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		//定义配置文件的路径
		String  xmlPath = "com/sun/static_factory/beans2.xml";
		//ApplicationContext在加载配置文件时，对Bean进行实例化
		ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPath);
		Bean2 bean =  (Bean2) ctx.getBean("bean2");
		System.out.println(bean);
	}
}
