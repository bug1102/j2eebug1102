package com.sun.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationAssembleTest {

	public static void main(String[] args) {
		//定义配置文件路径
		String xmlPtah = "com/sun/annotation/beans6.xml";
		//加载配置文件
		ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlPtah);
		//获取UserController实例
		UserController bean = (UserController) ctx.getBean("userController");
		//调用UserController中的save（）方法
		bean.save();
	}
}
