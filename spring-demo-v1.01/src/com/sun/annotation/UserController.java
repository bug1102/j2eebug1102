package com.sun.annotation;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

@Controller("userController")
public class UserController {

	@Resource(name="userService")
	private UserService userService;
	
	public void save() {
		userService.save();
		System.out.println("userController....save....");
	}
}
