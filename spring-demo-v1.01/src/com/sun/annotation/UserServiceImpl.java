package com.sun.annotation;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService{

//	@Resource(name="userDao") 要求name中的值必须与@Repository中的值相同
	@Autowired //根据类型进行注入
	private UserDao userDao;
	
	@Override
	public void save() {
		//调用userDao中的save方法
		this.userDao.save();
		System.out.println("userService....save...");
	}

}
