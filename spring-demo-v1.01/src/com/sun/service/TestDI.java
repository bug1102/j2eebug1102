package com.sun.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDI {

	
	public static void main(String[] args) {
		//1.初始化spring容器，加载配置文件
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.通过容器获取user Service实例
		UserService bean = (UserService) ctx.getBean("userService");
		//3.调用实例中的say方法
		bean.say();
	}
}
