package com.sun.service;

import com.sun.dao.UserDao;

public class UserServiceImpl implements UserService {

	//声明UserDao属性
	private UserDao userDao;
	
	public void setUserDao(UserDao userDas) {
		this.userDao = userDas;
	}
	
	@Override
	public void say() {
		//调用userDao中的say方法，执行输出
		this.userDao.say();
		
		
		System.out.println("userService say:Hello world lalala");
	}

}
